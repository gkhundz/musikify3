defmodule Musikify.SearchHistoryTest do
    use Musikify.DataCase
  
    alias Musikify.Repositories.SearchHistory
  
    describe "search_history_items" do
      alias Musikify.SearchHistoryItem
      alias Musikify.User

      @valid_attrs %{created_at: ~N[2010-04-17 14:00:00.000000], query: "some query"}

      def search_history_item_fixture(attrs \\ %{}, user \\ %{}) do
        {:ok, search_history_item} =
          attrs
          |> Enum.into(@valid_attrs)
          |> SearchHistory.create_search_history_item(user)
  
        search_history_item
      end

      test "create_search_history_item/1 with valid data creates a search_history_item" do
        {:ok, user} =         %User{}
        |> User.changeset(%{name: "user", age: 12})
        |> Repo.insert()

        assert {:ok, %SearchHistoryItem{} = search_history_item} = SearchHistory.create_search_history_item(@valid_attrs, user)
        assert search_history_item.created_at == ~N[2010-04-17 14:00:00.000000]
        assert search_history_item.query == "some query"
      end

      test "list_last_search_history_items_for_user/1 returns last 10 search_history_item for specific user_id" do

        {:ok, user} =  %User{}
                        |> User.changeset(%{name: "user", age: 12})
                        |> Repo.insert()

        
        res = 1..15
        |> Enum.map(fn i -> search_history_item_fixture(%{
            query: "query "<>Integer.to_string(i)
            }, user) end)

        
        assert length(SearchHistory.list_last_search_history_items_for_user(user.id) ) == 10

      end
      test "list_last_search_history_items_for_user/1 returns most recent search item first" do
        {:ok, user} = %User{}
                    |> User.changeset(%{name: "user", age: 12})
                    |> Repo.insert()

        res = 1..10
        |> Enum.map(fn i -> search_history_item_fixture(%{
            query: "query "<>Integer.to_string(i)
            }, user) end)

        history_item = search_history_item_fixture(%{query: "current"}, user)
        
        [ last_history_item | _ ] = SearchHistory.list_last_search_history_items_for_user(user.id)

        assert last_history_item == history_item
      end
      test "list_last_search_history_items_for_user/1 doesn't return history items if corresponding user doesn't exist" do
        
       
        
        assert SearchHistory.list_last_search_history_items_for_user(-1)  == []
      end


      test "delete history item deletes the history item specified by the user_id and the query" do
        {:ok, user} = %User{}
        |> User.changeset(%{name: "user", age: 12})
        |> Repo.insert()

        1..15
        |> Enum.map(fn i -> search_history_item_fixture(%{
        query: "query "<>Integer.to_string(i)
        }, user) end) 

        [ last_history_item_before_deletion | _ ] = SearchHistory.list_last_search_history_items_for_user(user.id)
        { numOfDeletedItems, _ } = SearchHistory.delete_search_history_item(user.id, last_history_item_before_deletion.id)
        [ last_history_item_after_deletion | _ ] = SearchHistory.list_last_search_history_items_for_user(user.id)
        assert last_history_item_after_deletion.id != last_history_item_before_deletion.id
        assert numOfDeletedItems == 1
      end

      test "delete_search_history_item wont delete the history item if the user_id specified wrong" do
        {:ok, user} = %User{}
        |> User.changeset(%{name: "user", age: 12})
        |> Repo.insert()

        1..15
        |> Enum.map(fn i -> search_history_item_fixture(%{
        query: "query "<>Integer.to_string(i)
        }, user) end) 
        
        [ last_history_item | _ ] = SearchHistory.list_last_search_history_items_for_user(user.id)
        { numOfDeletedItems, _ } = SearchHistory.delete_search_history_item(-1, last_history_item.id)
        assert numOfDeletedItems == 0
      end
    end

end