import {includes, takeRight, first, sampleSize} from "lodash"
import * as _ from "lodash"

let lastId = 2;
let history = [
    {id:"1", query:"sunny"},
    {id:"2", query:"sunday"},
];

let music = [
    {id:"1", name:"Sunday Morning", artist: "Maroon 5", released:"2002"},
    {id:"2", name:"Sunny", artist: "Boney M", released:"1976"},
    {id:"3", name:"Happier", artist: "Marshmello & Bastille", released:"2018"},
    {id:"4", name:"Closer", artist: "Halsey", released:"2016"},
    {id:"5", name:"Wonderwall", artist: "Oasis", released:"1995"},
    {id:"6", name:"Scientist", artist: "Coldplay", released:"2002"},
];

export class Storage {
    static findMusic(name){
        console.log(music.filter(n => includes(n.name, name)))
        return music.filter(n => includes(n.name.toLowerCase(), name.toLowerCase())
        || includes(n.artist.toLowerCase(), name.toLowerCase()) || includes(n.released.toLowerCase(), name.toLowerCase()));
    }
    static liveSearch(name){
        return takeRight(this.findMusic(name), 5).reverse();
    }
    static lastHistoryItems(){
        return takeRight(history, 5).reverse();
    }
    static insertHistoryItem(query){
        history.push({_id: ""+(++lastId), query:query});
    }
    static youMayLike(){
        return sampleSize(music, 3);
    }

};