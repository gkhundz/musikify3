
// First we import the various pieces of Vue and Apollo.
import Vue from 'vue'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import VueApollo from 'vue-apollo'
import VueRouter from 'vue-router'


// Import our main app component, the top-level container for our app. 
import App from './App.vue'
import Search from './Search.vue'

const routes = [
  { path: '/', component: App },
  { path: '/history', component: App },
  { path: '/search/:query', component: Search, name:"search" }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})

Vue.use(VueRouter);

// Bootstrap the new Vue app. It grabs onto the div with id="app that we created in the Phoenix HTML.
// We pass the apolloProvider instance to it, so our components can all use the same connection.
new Vue({router}).$mount("#app");
