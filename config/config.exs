# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :musikify,
  ecto_repos: [Musikify.Repo]

# Configures the endpoint
config :musikify, MusikifyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "idfW7klrBEXN1e9umjoYzbSno+OqAhxKF4m+i3Yq0lFDuh350JgTKpW4ekqwaTDi",
  render_errors: [view: MusikifyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Musikify.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
