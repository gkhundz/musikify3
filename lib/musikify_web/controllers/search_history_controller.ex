defmodule MusikifyWeb.SearchHistoryController do
  use MusikifyWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
