defmodule Musikify.SearchHistoryItem do
  use Ecto.Schema
  import Ecto.Changeset

  alias Musikify.User


  schema "search_history_items" do
    field :created_at, :naive_datetime
    field :query, :string
    # field :user_id, :id
    belongs_to :user, User
    # belongs_to :user, Musikify.User

    timestamps()
  end

  @doc false
  def changeset(search_history_item, attrs) do
    search_history_item
    |> cast(attrs, [:query, :created_at])
    |> validate_required([:query, :created_at])
  end
end
