defmodule Musikify.Song do
  use Ecto.Schema
  import Ecto.Changeset


  schema "songs" do
    field :artist, :string
    field :date, :string
    field :genre, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(song, attrs) do
    song
    |> cast(attrs, [:title, :genre, :artist, :date])
    |> validate_required([:title, :genre, :artist, :date])
  end
end
