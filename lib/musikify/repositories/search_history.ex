defmodule Musikify.Repositories.SearchHistory do
    @moduledoc """
    The SearchHistory context.
    """
  
    import Ecto.Query, warn: false
    alias Musikify.Repo
  
    alias Musikify.SearchHistoryItem
    alias Musikify.User

    @doc """
    Creates a search_history_item.

    ## Examples

        iex> create_search_history_item(%{field: value})
        {:ok, %SearchHistoryItem{}}

        iex> create_search_history_item(%{field: bad_value})
        {:error, %Ecto.Changeset{}}

    """
    def create_search_history_item(attrs \\ %{}, user \\ %{}) do
        %SearchHistoryItem{}
        |> SearchHistoryItem.changeset(attrs)
        |> Ecto.Changeset.put_change(:user_id, user.id)
        |> Repo.insert()
    end

    def list_last_search_history_items_for_user(user_id) do
        items = from shi in SearchHistoryItem,
          where: shi.user_id == ^user_id,
          order_by: [desc: shi.id],
          limit: 10,
          select: shi


        Repo.all(items)
      end
      def delete_search_history_item(user_id, id) do
        from(p in "search_history_items", where: p.user_id == ^user_id and p.id == ^id) |> Repo.delete_all()
      end
  end
  