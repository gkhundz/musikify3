defmodule Musikify do

  @moduledoc """
  Musikify keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias Musikify.Repo
  alias Musikify.Song

  def autocomplete_search(name) do
    songs = Repo.all(Song)
    s = for song <- songs, String.contains?(String.downcase(song.title), String.downcase(name)), do: song.title

    s|> Enum.take(10)
  end
end
