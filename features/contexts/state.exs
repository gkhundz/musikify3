defmodule GherkinTests.State do
  
    def store(state, key, value), do: Map.put(state, key, value)
    
    def retrieve(state, key), do: Map.get(state, key)
    
    def set_if_not_exists(state, key, value) do
      if Map.has_key?(state, key) do
        state
      else
        store(state, key, value)
      end
    end
  
  end