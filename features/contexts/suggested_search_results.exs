defmodule UserGetsSuggestedSearchResults do
    use WhiteBread.Context
    use Hound.Helpers
  
    when_ ~r/^I click the search input field$/, fn state ->
        {:ok, state}
    end
    
    then_ ~r/^I start writing title$/, fn state ->
        {:ok, state}
    end
    
    when_ ~r/^I see suggested item$/, fn state ->
        {:ok, state}
    end
    
    then_ ~r/^I click on item$/, fn state ->
        {:ok, state}
    end

    when_ ~r/^I am redirectred$/, fn state ->
        {:ok, state}
    end
        
    then_ ~r/^I see page of this title$/, fn state ->
        {:ok, state}
    end

    then_ ~r/^I don't see suggested item$/, fn state ->
        {:ok, state}
      end
  end