defmodule UserGetsSearchInputFilled do
    use WhiteBread.Context
    use Hound.Helpers
    # When I type in the search input field
    # Then I see search suggestions
    given_ ~r/^I went to website home page$/, fn state ->
      {:ok, state}
    end
  
    when_ ~r/^I type in the search input field$/, fn state ->
      {:ok, state}
    end
  
    then_ ~r/^I see search suggestions$/, fn state ->
      {:ok, state}
    end
    when_ ~r/^I click the blue arrow next to suggestion$/, fn state ->
      {:ok, state}
    end
  
    then_ ~r/^My search input fills with suggested text$/, fn state ->
      {:ok, state}
    end

  end