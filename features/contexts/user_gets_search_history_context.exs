defmodule UserGetsSearchHistoryContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias GherkinTests.State

  given_ ~r/^I went to website home page$/, fn state ->
    navigate_to "/"
    # :timer.sleep(6000)
    {:ok, state}
  end

  when_ ~r/^I click the search input field$/, fn state ->
    click {:id, "search_input"}
    # fill_field({:id, "search_input"}, "hello")
    # :timer.sleep(6000)
    {:ok, state}
  end

  then_ ~r/^I see the user search history$/, fn state ->
    {:ok, state}
  end

  when_ ~r/^I change input focus state to unfocused$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^I don't see search history block$/, fn state ->
    {:ok, state}
  end
end
