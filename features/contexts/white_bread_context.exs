defmodule WhiteBreadContext do
  use WhiteBread.Context

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end

  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end

  scenario_finalize fn _status, _state -> 
    Hound.end_session
  end

  import_steps_from UserGetsSearchHistoryContext
  import_steps_from UserGetsSuggestedSearchResults
  import_steps_from UserGetsSearchInputFilled
  import_steps_from UserGetsSearchSuggestions
end
