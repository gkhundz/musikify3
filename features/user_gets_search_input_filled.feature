Feature: User gets search input filled

  Scenario: Clicking blue arrow next to suggested search fills search input
    Given I went to website home page
    When I type in the search input field
    Then I see search suggestions
    When I click the blue arrow next to suggestion
    Then My search input fills with suggested text

