Feature: User gets suggested search results
    I will not be able to click untill there is no item


    Scenario: Pressing suggested search input Gives me Suggested items block
        Given I went to website home page
        When I click the search input field
        Then I start writing title
        When I see suggested item
        Then I click on item
        When I am redirectred
        Then I see page of this title

    Scenario: Going out of focus from search input hides Suggested items block
        Given I went to website home page
        When I click the search input field
        Then I start writing title
        When I change input focus state to unfocused
        Then I don't see suggested item