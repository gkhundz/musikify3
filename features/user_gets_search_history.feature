Feature: User gets search history

    Scenario: Pressing search input field Gives me Search History block
        Given I went to website home page
        When I click the search input field
        Then I see the user search history

    Scenario: Going out of focus from search input hides Search history block
        Given I went to website home page
        When I click the search input field
        Then I see the user search history
        When I change input focus state to unfocused
        Then I don't see search history block