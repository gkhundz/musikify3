# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Musikify.Repo.insert!(%Musikify.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Musikify.{Repo, Song}

[%{artist: "Rashid Behbutov", date: "03.12.1996", genre: "classical", title: "Ayriliq"},
%{artist: "Rammstein", date: "03.12.1996", genre: "metal", title: "Mein Hertz Brennt"},
%{artist: "Alina Orlova", date: "03.12.1996", genre: "folclore", title: "Menulis"},
%{artist: "Led Zeppelin", date: "03.12.1996", genre: "rock", title: "Stairways to Heaven"},
%{artist: "Duran Duran", date: "03.12.1996", genre: "rock", title: "Come Undone"},
%{artist: "Duman", date: "03.12.1996", genre: "rock", title: "Senden Daha Guzel"}
]
|> Enum.map(fn song_data -> Song.changeset(%Song{}, song_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)