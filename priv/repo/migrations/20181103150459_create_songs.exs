defmodule Musikify.Repo.Migrations.CreateSongs do
  use Ecto.Migration

  def change do
    create table(:songs) do
      add :title, :string
      add :genre, :string
      add :artist, :string
      add :date, :string

      timestamps()
    end

  end
end
